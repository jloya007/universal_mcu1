// Main_Siemens_Automation_824MSPe.h
#ifndef _MAIN_SIEMENS_AUTOMATION_824MSPE_h
#define _MAIN_SIEMENS_AUTOMATION_824MSPE_h

//Define Cpp Functions.

//Hydraulic Function.
void Hydraulic_Start_Function();
//Foot pedal Function.
void Footpedal_Function();
//Bypass Function.
void Bypass_Function();
//Force Function.
void Force_Function();
//Safety Sensor State Function.
void Safety_Sensor_State_Function();
//Alarm Function.
void Alarm_Function();
//Machine State Function.
void Machine_State_Function();
//Initialization Function.
void Initialization_Function();
//Timer Function.
void Timer_Function();
//Conductive Mode Function.
void Conductive_Mode_Function();
//Tooling contact Function.
void Toolingcontact_Function();
//Non Conductive Mode Function.
void Non_Conductive_Mode_Function();
//Laser Light Function.
void Laser_Light_Function();
//Work Light Function.
void Work_Light_Function();
//Buzzer Function.
void Buzzer_Function();
//Modbus Communication Function.
void Io_Poll();
//Setup Buzzer Function.
void Setup_Buzzer_Function();
//TPS Function.
void Tooling_Protection_System_Function();
//Hydraulic Cylinder Function.
void Hydraulic_Cylinder_Function();
//Safety Block Function.
void Safety_Block_Function();
//Top of stroke Function.
void TOS_Function();
//Setup Hydraulic Cylinder Function.
void Setup_Hydraulic_Cylinder_Function();
//Robot_Communication_Function.
void Robot_Communication_Function();
//Robot Lower Tool Pin Function.
void Robot_Lower_Tool_Pin_Function();
//MCU version function.
void MCU_Version_Function();
//Wire Communication Receive.
void Wire_Communication_Receive(int);

//Global Variables. Start definition at Main_Siemens_Automation_824MSPe cpp file.
extern bool Safety_Sensor_Error;
extern bool Tooling_Contact_Error;
extern bool Machine_Init_State;
extern bool Machine_Idle_State;
extern bool Machine_Run_State;
extern bool Toolingcontact_Switched_State;
extern bool Safety_Sensor_Switched_State;
extern bool Non_Conductive_Stop1;
extern bool Non_Conductive_Stop2;
extern bool TPS_Error;
extern bool Hyd_Cyl_Down_Release_Active;
extern bool Hyd_Cyl_Up_Release_Active;
extern int TOS;
extern bool SSSF_Safety_Sensor_Error;
extern bool EMG_Error_Reset_MCU1;
extern bool EMG_Insertion_machine_Active;
extern int Analog_CET;
extern int MCU1_Version_Number;

//Global Variables from or Set to MCU2.
extern bool Robot_Mode_Active_MCU2;
extern bool Robot_Error_Reset_MCU2;
extern bool Error_Set_HMI_MCU2;
extern bool EMG_Error_Reset_MCU2;
extern bool EMG_System_Active_MCU2;
extern bool All_At_Position_MCU2;
extern bool Press_Complete_MCU2;
extern bool Robot_Press_Active_MCU2;
extern bool Lower_Tool_Pin_Down_Sensor_MCU2;
extern bool Lower_Tool_Pin_Up_Sensor_MCU2;
extern bool Robot_Feed_Fastener_MCU2;
extern bool Robot_Shuttle_Lock_MCU2;
extern bool Up_Signal_MCU2;
extern bool Down_Solenoid_Block_MCU2;
extern bool Robot_Error_Reset_HMI;

//Modbus HMI Variables.
extern bool Hydraulic_Enable_HMI;
extern bool ConductiveM_HMI;
extern bool Tooling_Contact_Error_HMI;
extern bool Laser_Light_HMI;
extern bool Work_Light_HMI;
extern bool MAS_Selected_HMI;
extern int Force_Set_Value_HMI;
extern bool Buzzer_HMI;
extern bool EMG_System_Active_HMI;
extern bool Hydraulic_Active;
//Upper Bypass Value. Cylinder will slow down passing (above) this point.
extern int Bypass_Upper_Value_HMI;
//Lower Bypass Value. Cylinder will slow down passing (below) this point.
extern int Bypass_Lower_Value_HMI;
//Upper TPS Value.
extern int TPS_Calibrated_Upper_Value_HMI;
//Lower TPS Value.
extern int TPS_Calibrated_Lower_Value_HMI;
//TPS offset value.
extern int TPS_Offset_Value_HMI;
//Error Reset.
extern bool Error_Reset_HMI;
//Top Of Stroke Position.
extern int TOS_Position_HMI;
//Buzzer Number Beeps HMI.
extern int Buzzer_Number_Beeps_HMI;
//Buzzer Beep Time HMI.
extern int Buzzer_Beep_Time_HMI;
//Buzzer Silent Time HMI.
extern int Buzzer_Silent_Time_HMI;
//Modbus Coil 4 Read.
extern int Read_Coil_4;
//Modbus Coil 4 Write.
extern int Write_Coil_4;
//Setup Stroke HMI.
extern bool Setup_Stroke_HMI;
//Fastener Length Calibration Value HMI.
extern int Fastener_Length_Calibrated_Value_HMI;
//Fastener Length HMI.
extern bool Fastener_Length_HMI;
//Auto Tooling HMI.
extern int Tooling_Mode_HMI;
//Error_Screen_Active_HMI
extern bool Error_Screen_Active_HMI;

//Robot Variables.
//Robot Eject Signal.
extern bool Robot_MAS_Eject;
//Robot Program_Byte
extern int Robot_Program_Byte;
//HMI Program Byte.
extern int Program_Byte_From_HMI;
//Program Byte from HMI.
extern int Robot_Program_ID_To_HMI;
//CET Output to Robot.
extern int Robot_CetValue_Output;
//Pressure Switch Output to Robot.
extern int Robot_Pressure_Switch_Output;
//Robot Lower Tool Pin Up Input.
extern bool Robot_Lower_Tool_Pin_Up;
//Robot Lower Tool Pin Down Input.
extern bool Robot_Lower_Tool_Pin_Down;
//MCU Lower Tool Pin Up movement Output.
extern bool MCU_Lower_Tool_Pin_Up_Output;
//MCU Lower Tool Pin Down movement Output.
extern bool MCU_Lower_Tool_Pin_Down_Output;
//MCU Lower Tool Pin Sensor Up Input.
extern bool MCU_Lower_Tool_Pin_Sensor_Up_Input;
//MCU Robot Lower Tool Pin Sensor Up Output.
extern bool Robot_Lower_Tool_Pin_Sensor_Up_Output;
//Robot Lower Tool Pin Sensor Down Output.
extern bool MCU_Lower_Tool_Pin_Sensor_Down_Input;
//Robot Lower Tool Pin Sensor Down Output.
extern bool Robot_Lower_Tool_Pin_Sensor_Down_Output;
//MCU Robot/Manual Key switch input at MCU1.
extern bool MCU_Mode_Key_Switch_Input1;
//MCU Robot/Manual Key switch input at MCU1.
extern bool MCU_Mode_Key_Switch_Input2;
//Robot Mode Active Output Active.
extern bool Robot_Mode_Active_Output1;
//Robot Mode Active Output Active.
extern bool Robot_Mode_Active_Output2;
//Robot Error Reset Input.
extern bool Robot_Error_Reset_Input;
//Robot Error Set Output (To Robot).
extern bool Robot_Error_Set;
//External E-Stop Robot Input #1.
extern bool E_Stop_EXT_1_Robot;
//External E-Stop Robot Input #2.
extern bool E_Stop_EXT_2_Robot;
//Robot EMG Error Reset.
extern bool Robot_EMG_Error_Reset;
//EMG Error output from Insertion machine to Robot.
extern bool Robot_EMG_Error_Set;
//All At Position Signal to Robot.
extern bool Robot_All_At_Position;
//Send Press Complete Signal to Robot from MCU2.
extern bool Robot_Press_Complete;
//Press signal is Active.
extern bool Press_Active_MCU1;
//Press Signal Input from Robot.
extern bool Robot_Press_Signal;
//Robot Press Active.
extern bool Robot_Press_Active_MCU1;
//Activate Hydraulic by Robot Output.
extern bool Robot_Hydraulic_Enabled_Input;
//Internal variable to Activate Hydraulic.
extern bool Robot_Hydraulic_Enable;
//Active Robot Mode.
extern bool Robot_Mode_Active_MCU1;
//Robot Feed Signal.
extern bool Robot_MAS_Feed_Signal;
//Robot Input to Lock Shuttle.
extern bool Robot_Shuttle_Lock;
// Dwell Time from MCU2.
extern bool Dwell_Time_Active_MCU2;
//Setup Stroke send to MCU2.
extern bool Setup_Stroke_MCU2;
//Shuttle Retract Output to Robot.
extern bool Shuttle_Retract_MCU1;
//Shuttle Extend Output to Robot.
extern bool Shuttle_Extend_MCU1;
//Shuttle Retract Signal from MCU2.
extern bool Shuttle_Retract_MCU2;
//Shuttle Extend Signal from MCU2.
extern bool Shuttle_Extend_MCU2;
//TOS output to Robot to ensure Robot knows that Hydraulic cylinder is at TOS.
extern bool Hydraulic_Cylinder_TOS_Robot;

//Global Timer Variables.
extern bool Timer1_Q;
extern bool Timer1_Start;

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#endif

